import 'package:flutter/material.dart';

class NewTransaction extends StatefulWidget {
  final Function handleTx;
//  final ctx;

  NewTransaction({this.handleTx});

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final titleController = TextEditingController();
  final amountController = TextEditingController();

  void _handle() {
    final enteredTitle = titleController.text;
    final enteredAmount = double.parse(amountController.text);

    if (enteredTitle.isEmpty || enteredAmount <= 0) {
      return;
    }

    widget.handleTx(enteredTitle, enteredAmount);

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(labelText: 'название покупки'),
              controller: titleController,
              onSubmitted: (_) => _handle(),
            ),
            TextField(
              decoration: InputDecoration(labelText: 'стоимость покупки'),
              controller: amountController,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              onSubmitted: (_) => _handle(),
            ),
            FlatButton(
              child: Text('Добавить покупку'),
              textColor: Colors.purple,
              onPressed: _handle,
            )
          ],
        ),
      ),
    );
  }
}
